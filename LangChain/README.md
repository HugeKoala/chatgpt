# ChatGPT API を使用して RAG を実装してみる

## 環境設定

- docker-compose up -d --build
- docker-compose exec langchain bash
- poetry install
- poetry shell

## 実行

- cd src
- python {main|sample_langchain}.py

## 質問を変更して実行する場合

{prompt|prompt_rag}.yml の questions を変更する

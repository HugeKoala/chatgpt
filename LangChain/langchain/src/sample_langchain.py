import yaml
from langchain.prompts import PromptTemplate
from langchain.chat_models import ChatOpenAI
from langchain.chains import LLMChain

# OpenAIのモデルのインスタンスを作成
chat = ChatOpenAI(model_name="gpt-3.5-turbo", temperature=0)

# yaml読み込み
with open("../yaml/prompt.yml", "r") as yml:
    config = yaml.safe_load(yml)

# 文章テキスト読み込み
with open('../data/input.txt', 'r', encoding='UTF-8') as f:
    context = f.read()

# プロンプトのテンプレート文章を定義
prompt_template = config["prompt_template"]

# テンプレート文章にあるチェック対象の単語を変数化
prompt = PromptTemplate(
    input_variables=["context", "question"],
    template=prompt_template,
)

# OpenAIのAPIにこのプロンプトを送信するためのチェーンを作成
chain = LLMChain(llm=chat, prompt=prompt,verbose=False)

answers = []

for i in range(1, len(config["questions"])+1):
    # チェーンを実行し、結果を表示
    answer = chain.run(
        {"context": context,
        "question": config["questions"][f"question_{i}"]
        })
    
    answers.append(answer)

print(answers)
import rag

if __name__ == "__main__":
    # rag実施
    instance = rag.RunRag()

    # 初回のみ実行
    # instance.make_db()

    # パス設定
    work_dir = instance.work_dir
    data_dir = instance.data_dir
    yaml_dir = instance.yaml_dir
    # yaml読み込み
    config = instance.config
    # DB読み込み
    database_df = instance.database_df

    # CPTに生成させる
    for i in range(1, len(config["questions"])+1):
        ask_text = config["questions"][f"question_{i}"]
        print("【質問】")
        print(ask_text)
        instance.rag_llm(instance.embeddings, ask_text, database_df)
        print("-------------------")
from langchain.embeddings import HuggingFaceEmbeddings
from langchain.prompts import PromptTemplate
from langchain.chat_models import ChatOpenAI
from langchain.chains import LLMChain
import pandas as pd
import numpy as np
import yaml
from pathlib import Path
from sklearn.metrics.pairwise import cosine_similarity
import warnings
warnings.filterwarnings('ignore')
import os
os.environ["TOKENIZERS_PARALLELISM"] = "false"

class RunRag():  
    def __init__(self):
        self.work_dir = Path().resolve().parent
        self.data_dir = self.work_dir / "data"
        self.yaml_dir = self.work_dir / "yaml"
        # モデル読み込み
        self.embeddings = HuggingFaceEmbeddings(model_name="intfloat/multilingual-e5-base")
        # OpenAIのモデルのインスタンスを作成
        self.chat = ChatOpenAI(model_name="gpt-3.5-turbo", temperature=0)
        # yaml読み込み
        with open(self.yaml_dir / "prompt_rag.yml", "r") as yml:
            self.config = yaml.safe_load(yml)
        # DB読み込み
        self.database_df = pd.read_csv(self.data_dir / "DB.csv")
        self.database_df["vector"] = list(np.load(self.data_dir / "DB.npz")["arr_0"])

    def make_db(self):
        # Embeddingの作成
        sample_text1 = """Amazon SageMakerなどAWSの機械学習サービスを活用したシステムの導入支援を行います。
        AIや機械学習は、マーケティングから小売や流通、製造など様々なビジネスシーンで活用されています。一方で、データサイエンティストや機械学習を熟知したエンジニアがいない、機械学習のシステム部分は専門家におまかせして予測や分析など業務に集中したいといった悩みや課題も少なくありません。
        機械学習やAWSの専門知識と豊富な導入実績を持つクラスメソッドが、お客様の機械学習システムの構築・導入を支援します。
        """
        doc_result1 = self.embeddings.embed_documents([sample_text1])

        sample_text2 = """機械学習に強いクラスメソッドにおまかせください
        レコメンド機能を活用しひとりひとりにあったコンテンツを提供することで、ユーザ滞在時間アップ、売上アップをはかります。
        豊富な導入実績を持つクラスメソッドが、お客様のデータ検証から本番環境の構築までをサポートします。
        """
        doc_result2 = self.embeddings.embed_documents([sample_text2])

        sample_text3 = """東京都（とうきょうと、英語: Tokyo Metropolis）は、日本の首都[1][2][注釈 2]。関東地方に位置する都[1][2]。都庁所在地は新宿区[1][注釈 3]。
        区部（特別区23区）、多摩地域（26市と西多摩郡3町1村）および島嶼部（2町7村）からなる。関東南西部にあって東西に細長い都域を有し、東部は東京湾に面する[2]。西部は雲取山を最高峰とする関東山地となる[3]。
        行政機関、金融機関や大企業などが集中し、新聞・放送・出版などの文化面、大学・研究機関などの教育・学術面においても日本の中枢をなす。交通面でも鉄道網、道路網、航空路の中心。
        東京都と周辺7県で首都圏を構成している。特に東京圏（東京都・神奈川県・千葉県・埼玉県）の総人口は約3500万人に達し、日本の人口の約30%が集中している[4]。東京都市圏としては世界最大級の人口を有する国際的大都市である[2]。
        """
        doc_result3 = self.embeddings.embed_documents([sample_text3])

        text_list = [sample_text1, sample_text2, sample_text3]
        vector_list = [doc_result1, doc_result2, doc_result3]
        database_df = pd.DataFrame({
            "text": text_list,
            "vector": vector_list
            }
        )

        # ベクトルデータ、元テキストをCSV・ベクトルをnpzファイルで保存
        database_df.to_csv(self.data_dir / "DB.csv")
        np.savez(self.data_dir / "DB",vector_list)

        print("DB作成が完了しました")

    def calc_cosine_similarity(self, database_vector, input_vector):
        return cosine_similarity(database_vector, input_vector)

    def make_runking_score(self, embeddings, input_text, database_df, K):
        # input_textをembedding
        doc_result = self.embeddings.embed_documents([input_text])
        
        # databaseとの近似度を算出
        database_df["score"] = database_df["vector"].apply(self.calc_cosine_similarity, input_vector=doc_result)

        # score順に並び替え
        database_df = database_df.sort_values(by="score", ascending=False).reset_index(drop=True)
        
        # 上位K件のtextをreturn
        return database_df["text"].tolist()[:K]

    def rag_llm(self, embeddings, ask_text, database_df):
        # プロンプトのテンプレート文章を定義
        prompt_template = self.config["prompt_template"]
       
        # DBから質問文に近しいテキストを抽出する
        context = self.make_runking_score(embeddings, ask_text, database_df, 2)

        # テンプレート文章にあるチェック対象の単語を変数化
        prompt = PromptTemplate(
            input_variables=["context_1", "context_2", "question"],
            template=prompt_template,
        )

        # OpenAIのAPIにこのプロンプトを送信するためのチェーンを作成
        chain = LLMChain(llm=self.chat, prompt=prompt,verbose=False)
        answer = chain.run(
            {"context_1": context[0],
            "context_2": context[1],
            "question": ask_text
            })
        
        print("【生成結果】")
        print(answer)